::Security::
	MalwareBytes (Antivirus):		https://www.malwarebytes.com/


::Internet::
	Firefox (Web Browser):
	Discord (Communication Service):		https://discord.com/
	Signal


::Quality of Life::
	F.lux / LightBulb (Blue Light):			https://justgetflux.com/	/	https://github.com/Tyrrrz/LightBulb
	CopyQ / ClipX (Clipboard History Manager):	https://hluk.github.io/CopyQ/	/	https://bluemars.org/clipx/


::Troubleshooting::	
	CrystalDiskInfo(disk health analyzer&info):		https://crystalmark.info/en/software/crystaldiskinfo/
	CrystalDiskMark(disk benchmark):		https://crystalmark.info/en/software/crystaldiskmark/
	HWiNFO (Hardware Info; Better taskmgr.exe):	https://www.hwinfo.com/
	Hiren's Boot CD: https://www.hirensbootcd.org/download/
	Microsoft Activation Scripts:  `irm https://get.activated.win | iex`   /   https://github.com/massgravel/Microsoft-Activation-Scripts


::Files::
	Everything (Windows Search Engine): https://voidtools.com/
	AntRenamer (Batch Renamer):
	WizTree	(Disk Space Analyzer):		https://diskanalyzer.com/
	Total Commander	(File Explorer):
	OSFMount (Disk-Image Mounter): 		https://www.osforensics.com/tools/mount-disk-images.html


::Downloader::
	qbittorrent enhanced(Torrent Client): https://github.com/c0re100/qBittorrent-Enhanced-Edition/releases
	soulseekqt (File Sharing Program): https://www.slsknet.org/news/node/1
	XDM (Xtreme Download Manager): 
	Ultimate Windows Utility:   `irm christitus.com/win | iex`   /   https://github.com/ChrisTitusTech/winutil


::Music::
	MusikCube (CLI Music Player): https://github.com/clangen/musikcube/releases
	MusicBee (GUI Music Player): https://getmusicbee.com/downloads/
	
	+Midi Shit:
		CoolSoft VirtualMidiSynth:
		CoolSoft MidiMapper:
		VanBasco's Karaoke Player:
	+Tracker:
		SunVox (Modular Synth + Tracker): https://warmplace.ru/soft/sunvox/
		
::Pictures/Video::
	Grafx2 (Art Software):
	Gimp  (Art Software):
	MyPaint (Art Software):
	MPV (Video Player):
	OBS (Video Recording and Live Streaming):
	
::Books::
	Calibre ():
	ComicRack ():
	
::Games::
	Playnite (Game Library Organizer): https://playnite.link/

	+Platforms:
		Steam
		GOG
		Itch.io
	
	+Emulators:
		=Console:
			Dolphin (GC/Wii):
			DuckStation (PS1):
			PCSX2 (PS2):
			Redream|Flycast|Reicast (DC):
		
		=PC:
			DosBox	(DOS): 
			SCUMMVM (Scumm):
			WinUAE	(Amiga):
			WineVDM / otvdm (16-bit Windows):

	Notable PC Ports:
		OpenGOAL (Jak & Daxter)
		SM64 PC Port
		Ship of Harkinian (OoT PC Port)
		
::Programming::
	Visual Studios:
	Jetbrains IDEs (Pycharm, IntelliJ, CLion):
	VSCode (between Notepad++ & an IDE):
	Notepad++ (Notepad for programmers):		https://notepad-plus-plus.org/
	Vim / Vim Enhanced

